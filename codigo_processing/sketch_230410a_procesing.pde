import processing.serial.*;

Serial myPort;  //Crea un objeto de clase serial
String val;     //Datos recibidos por el Puerto serial

void setup () {
  size(400, 300);
 
  String portName = Serial.list()[0]; 

  myPort = new Serial(this, portName, 9600);
}

void draw() {
if ( myPort.available() >0){  //If data is available,  
  val = myPort.readStringUntil('\n');  //Lo guarda en val  
  } 
if(val!=null){
  if(float(val)<25){
    background(0,80,200);
    textSize(24);
    String textoFrio="Temperatura demasiado baja\n"+val;
    text(textoFrio, 80, 130);
  }else if(float(val)>26){
    background(200,80,0);
    textSize(24);
    String textoCaliente="Temperatura demasiado alta\n"+val;
    text(textoCaliente, 80, 130);
  }else{
    background(0,200,80);
    textSize(30);
    String textoNeutral="Temperatura ideal\n"+val;
    text(textoNeutral, 80, 130);
  }
 }
 delay(2000);
}
