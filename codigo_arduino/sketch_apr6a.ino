#define led1 9  //R
#define led2 10 //G
#define led3 11 //B
//sensor dht11
#include <DHT.h>
#define DHTTYPE DHT11
#define DHTPIN 2

//Relé
#define ENABLE 13

DHT dht(DHTPIN, DHTTYPE);

void setup(){
  pinMode(ENABLE,OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  Serial.begin(9600);
  dht.begin();
}

void loop(){

  // Leemos la temperatura en grados centígrados (por defecto)
  float t = dht.readTemperature();
  //float t = 16;
  float limiteInferior=25.0;
  float limiteSuperior=26.0;
 
  // Comprobamos si ha habido algún error en la lectura
  if (isnan(t)) {
    Serial.println("Error obteniendo los datos del sensor DHT11");
    return;
  }
  
  Serial.println(t);
 
  if(t<limiteInferior){
  //azul
  digitalWrite(led1, LOW);
  digitalWrite(led2, HIGH);
  digitalWrite(led3, LOW);
  digitalWrite(ENABLE,HIGH);
  delay(500);

  }else if(t>limiteSuperior){
    //Rojo
  digitalWrite(led1, HIGH);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(ENABLE,LOW); 
  delay(500);
  }else{
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, HIGH);
    delay(500);
  }
  delay(5000);

}